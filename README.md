# google-analytics-go

## Instructions
Add the json credential file to an environment argument called GAGO_AUTH in your terminal.
```
export GAGO_AUTH=<PATH_TO_YOUR_GA_JSON_CREDENTIALS
```

Download module dependencies.
```
go mod download
```

Run the application.
```
go run main go
```

## Documentation

- https://github.com/MarkEdmondson1234/gago
- https://code.markedmondson.me/gago/
