package main

import (
	"github.com/MarkEdmondson1234/gago/gago"
	"os"
)

// How to fetch all rows of data with the unsampled flag set that breaks up your API calls into batches under the sampling limits
func main()  {
	// get auth file and authenticate
	authFile := os.Getenv("GAGO_AUTH")
	analyticsreportingService, _ := gago.Authenticate(authFile)

	// make report request struct
	var req = gago.GoogleAnalyticsRequest{
		Service:    analyticsreportingService,
		ViewID:     "154884107",
		Start:      "2020-01-07",
		End:        "2020-05-14",
		Dimensions: "ga:userAgeBracket",
		Metrics:    "ga:users",
		MaxRows:    -1,
		AntiSample: true}

	// API calls
	report := gago.GoogleAnalytics(req)

	// write data out as a CSV to console
	gago.WriteCSV(report, os.Stdout)
}

// To start with, the v4 feature of batching is used, meaning five API requests can be made at once.
// you can fetch multiple batched requests at once, with a limit of ten per web property - \
// Go make it easy to set off ten requests at once and monitor and patch them together again.
// This means 10 API requests * 5 batched = 50 API pages can be requested at once.